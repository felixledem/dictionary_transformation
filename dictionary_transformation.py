"""
Time-Complexity: O(N*M)
M is size of dictionary, N is size of word
Explanation: worst-case, each node in graph is visited once during bi-directional BFS: O(M)
for each node, 26 * N permutations are checked: O(N)

"""


import string
import json
import sys


class Graph:
    """Word Graph used to search for paths between words.
    This graph is grown layer by layer. With each growth,
    a new set of words to visit is created.

    Attributes:
        dictionary (set): contains all words in dictioanry
        to_visit (set): contains words to be visited
        visited (set): contains words that have been visited
    """

    def __init__(self, dictionary, word):
        """Initiates first word to visit in graph"""

        self.dictionary = dictionary
        self.to_visit = set([word])
        self.visited = set([word])

    def grow(self):
        """Grows graph from words in to_visit by one extra layer"""

        to_visit_new = set([])
        for word in self.to_visit:
            for neighbor in self.neighbor_generator(word):
                if all(neighbor not in i for i in (self.visited, self.to_visit)):
                    to_visit_new.add(neighbor)
            self.visited.add(word)
        self.to_visit = to_visit_new

    def neighbor_generator(self, word):
        """Generates all neighbors of a word."""
        for i in range(len(word)):
            for c in string.lowercase:
                new_word = word[:i] + c + word[1+i:]
                if new_word in self.dictionary:
                    yield new_word


def num_of_jumps(first_word, last_word, dictionary):

    if len(last_word) != len(first_word):
        return -1

    if len(first_word) < 1 or len(last_word) < 1:
        return -1

    if not (first_word in dictionary and last_word in dictionary):
        return -1

    # start building graph around first word and last word
    g1 = Graph(dictionary, first_word)
    g2 = Graph(dictionary, last_word)
    distance = 0

    # Bi-directional BFS
    while(True):
        if g1.to_visit == set([]) or g2.to_visit == set([]):
            # BFS is over, words visited and no path exists
            return -1

        if not g2.to_visit.isdisjoint(g1.to_visit):
            return distance
        g1.grow()
        distance += 1

        if not g2.to_visit.isdisjoint(g1.to_visit):
            return distance
        g2.grow()
        distance += 1
    return distance


if __name__ == "__main__":
    """args must be path to json file"""

    path = sys.argv[1]
    try:
        with open(path) as data_file:
            data = json.load(data_file)
        print "The shortest transformation is of length %i" % num_of_jumps(**data)
    except Exception, e:
        print "Please check your json and your file path"
