from dictionary_transformation import num_of_jumps
import random
import time
# import your_module


def time_it(f1, f2, d, select_from):
    sum1 = 0
    sum2 = 0

    for i in range(10):
        word1 = random.choice(select_from)
        word2 = random.choice(select_from)
        t1 = time.clock()
        for i in range(10):
            f1(word1, word2, d)
        t2 = time.clock()
        for i in range(10):
            f2(word1, word2, d)
        t3 = time.clock()
        sum1 += (t2 - t1)
        sum2 += (t3 - t2)
    return sum1, sum2


with open("words.txt") as f:
    d = set([])
    for line in f:
        d.add(line.strip())

    d5 = filter(lambda x: len(x) == 5, d)
    # gets 5 letter words only

    # replace num_of_jumps with your function here
    # t1, t2 = time_it(num_of_jumps, your_module.your_function, d, d5)
    t1, t2 = time_it(num_of_jumps, num_of_jumps, d, d5)

    print "My function took: %fs " % t1
    print "Your function took: %fs" % t2
