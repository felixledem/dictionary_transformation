import unittest
from dictionary_transformation import num_of_jumps


class DictionaryTransformationTest(unittest.TestCase):

    def test_num_of_jumps(self):
        d = {"hit", "dot", "dog", "hot", "cog", "log"}
        self.assertEqual(4, num_of_jumps("hit", "cog", d))

    def test_no_path(self):
        d = {"abc", "abd", "acd", "bce", "bde", "cde"}
        self.assertEqual(-1, num_of_jumps("abc", "cde", d))

    def test_small_dictionary(self):
        d = {"abc"}
        self.assertEqual(-1, num_of_jumps("abc", "abd", d))

    def test_not_in_dictionary(self):
        d = {"abc", "abd"}
        self.assertEqual(-1, num_of_jumps("abc", "abe", d))

    def test_not_same_length_dict(self):
        d = {"abc", "abcd", "abcde", "abcdef", "abd"}
        self.assertEqual(1, num_of_jumps("abc", "abd", d))

    def test_not_same_length_word(self):
        d = {"abc", "abd", "acd", "bce", "bde", "cde"}
        self.assertEqual(-1, num_of_jumps("abc", "abcd", d))

    def test_not_same_length_word_2(self):
        d = {"abc", "abd", "acd", "bce", "bde", "cde", "abcd"}
        self.assertEqual(-1, num_of_jumps("abc", "abcd", d))

    def test_same_word(self):
        d = {"abc", "abd", "acd", "bce", "bde", "cde"}
        self.assertEqual(0, num_of_jumps("abc", "abc", d))

    def test_one_word_away(self):
        d = {"abc", "abd", "acd", "bce", "bde", "cde"}
        self.assertEqual(1, num_of_jumps("abc", "abd", d))

    def test_two_paths(self):
        d = {"abc", "abd", "acc", "acd", "bce", "bde", "cde"}
        self.assertEqual(2, num_of_jumps("abc", "acd", d))

    def test_repetion_in_dictionary(self):
        d = {"abc", "abc", "abd", "acd", "bce", "bde", "cde"}
        self.assertEqual(2, num_of_jumps("abc", "acd", d))

    def test_large_dict(self):
        with open("words.txt") as f:
            d = set([])
            for line in f:
                d.add(line.strip())
        self.assertEqual(3, num_of_jumps("fog", "hey", d))


if __name__ == '__main__':
    unittest.main()
